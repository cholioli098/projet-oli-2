#include "produit.h"

Produit::Produit()
{
}

#pragma region Getters et setters

QString Produit::obtenirId() {
    return id;
}

QString Produit::obtenirNom() {
    return nom;
}

QString Produit::obtenirDescription() {
    return description;
}

QString Produit::obtenirCouleur() {
    return couleur;
}

double Produit::obtenirPrixUnitaire() {
    return prixUnitaire;
}

int Produit::obtenirQuantiteInventaire() {
    return quantiteInventaire;
}

bool Produit::obtenirActif() {
    return actif;
}

void Produit::definirId(QString _id) {
    id = _id;
}

void Produit::definirNom(QString _nom) {
    nom = _nom;
}

void Produit::definirDescription(QString _description) {
    description = _description;
}

void Produit::definirCouleur(QString _couleur) {
    couleur = _couleur;
}

void Produit::definirPrixUnitaire(double _prixUnitaire) {
    prixUnitaire = _prixUnitaire;
}

void Produit::definirQuantiteInventaire(int _quantiteInventaire) {
    quantiteInventaire = _quantiteInventaire;
}

void Produit::definirActif(bool _actif) {
    actif = _actif;
}

#pragma endregion


QVector<Produit> Produit::obtenirTousLesProduits()
{
    QVector<Produit> produits;
    QString requeteString = "SELECT * FROM produit order by nom;";
    QSqlQuery requete;

    if (!requete.exec(requeteString)) {
        qWarning() << "ERROR: " << requete.lastError().text();
    }
    while (requete.next())
    {
        Produit produit;
        produit.definirId(requete.value(0).toString());
        produit.definirNom(requete.value(1).toString());
        produit.definirDescription(requete.value(2).toString());
        produit.definirCouleur(requete.value(3).toString());
        produit.definirPrixUnitaire(requete.value(4).toDouble());
        produit.definirQuantiteInventaire(requete.value(5).toInt());
        produit.definirActif(requete.value(6).toBool());
        produits.push_back(produit);
    }
    return produits;
}

// Pas test�. Juste ajout� le code.
// Ajouter la validation de s'il y a assez de quantit� en inventaire pour retirer la quantit� pass�e en param�tre au besoin.
bool Produit::retirerQuantiteInventaire(int quantiteARetirer)
{
    QString requeteString = "UPDATE produit SET quantite_inventaire = (quantite_inventaire - ?) WHERE id = ?;";
    QSqlQuery requete;
    requete.prepare(requeteString);
    // TODO Valider avant
    requete.addBindValue(quantiteARetirer);
    requete.addBindValue(id);

    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
        return false;
    }

    quantiteInventaire = quantiteInventaire - quantiteARetirer;
    return true;
}

// Pas test�. Juste �crit le code.
bool Produit::enregistrer()
{
    QString requeteString = "INSERT INTO produit(nom, description, couleur, prix_unitaire, quantite_inventaire, actif) VALUES (?, ?, ?, ?, ?, ?);";
    QSqlQuery requete;
    requete.prepare(requeteString);
    // TODO Valider avant
    requete.addBindValue(nom);
    requete.addBindValue(description);
    requete.addBindValue(couleur);
    requete.addBindValue(prixUnitaire);
    requete.addBindValue(quantiteInventaire);
    requete.addBindValue(actif);

    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
        return false;
    }

    id = requete.lastInsertId().toString();
    return true;
}

Produit::~Produit()
{
}