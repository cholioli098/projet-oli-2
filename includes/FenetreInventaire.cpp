#include "FenetreInventaire.h"

AjouterProduit::AjouterProduit(QWidget(*parent)) : QWidget(parent) {

	QVBoxLayout* colonne1 = new QVBoxLayout(this);

	//Rang� de lbl
	QHBoxLayout* lblLigne1 = new QHBoxLayout(this);
	QLabel* lblNom = new QLabel("Nom");
	QLabel* lblCouleur = new QLabel("Couleur");
	lblLigne1->addWidget(lblNom);
	lblLigne1->addWidget(lblCouleur);
	colonne1->addLayout(lblLigne1);


	//lineEdit 
	QHBoxLayout* ligne1 = new QHBoxLayout(this);
	nom = new QLineEdit(this);
	nom->setText("Nom du produit");
	ligne1->addWidget(nom);
	couleur = new QLineEdit(this);
	couleur->setText("Couleur du produit");
	ligne1->addWidget(couleur);
	colonne1->addLayout(ligne1);

	//les label
	QHBoxLayout* lblLigne2 = new QHBoxLayout(this);
	QLabel* lblQuantite = new QLabel("Quantite");
	lblLigne2->addWidget(lblQuantite);
	QLabel* lblPrix = new QLabel("Prix");
	lblLigne2->addWidget(lblQuantite);
	colonne1->addLayout(lblLigne2);

	//lineEdit qt et prix
	QHBoxLayout* ligne2 = new QHBoxLayout(this);
	quantite = new QComboBox(this);
	for (int i = 1; i < 100; i++) {
		quantite->addItem(QString::number(i));
	}
	ligne2->addWidget(quantite);
	prix = new QLineEdit(this);
	prix->setText("Prix du produit");
	ligne2->addWidget(prix);
	colonne1->addLayout(ligne2);

	//bouton de fin
	QHBoxLayout* ligne3 = new QHBoxLayout(this);
	btnAjouter = new QPushButton(this);
	btnAjouter->setText("Ajouter le produit");
	ligne3->addWidget(btnAjouter);
	btnBack = new QPushButton(this);
	btnBack->setText("Retourner");
	ligne3->addWidget(btnBack);
	colonne1->addLayout(ligne3);

	setLayout(colonne1);

	connect(btnBack, SIGNAL(clicked()), parent, SLOT(allerIG()));
	//manque la fonction pour ajouter le produit a la bd et la firmation
	connect(btnAjouter, SIGNAL(clicked()), this, SLOT(ajouterProduit()));

}

void AjouterProduit::ajouterProduit() {
	//ajouter a la bd et demander si on fait un autre produit ou quitte on menu
}

AjouterQuantite::AjouterQuantite(QWidget* parent) : QWidget(parent) {
	QVBoxLayout* colonne1 = new QVBoxLayout(this);

	//Rang� de lbl
	QHBoxLayout* lblLigne1 = new QHBoxLayout(this);
	QLabel* lblNom = new QLabel("Nom");
	QLabel* lblCouleur = new QLabel("Couleur");
	lblLigne1->addWidget(lblNom);
	lblLigne1->addWidget(lblCouleur);
	colonne1->addLayout(lblLigne1);

	//premiere rang�
	QHBoxLayout* ligne1 = new QHBoxLayout(this);
	nom = new QComboBox(this);
	nom->addItem("");
	// faire une boucle et ajouter les nom de produit
	nom->addItem("bas");
	nom->addItem("bavette");

	couleur = new QComboBox(this);
	couleur->addItem("");
	// faire une boucle et ajouter les couleur avec le meme nom du produit
	couleur->addItem("rouge");
	couleur->addItem("bleu");


	ligne1->addWidget(nom);
	ligne1->addWidget(couleur);
	colonne1->addLayout(ligne1);

	//Rang� de lbl
	QHBoxLayout* lblLigne2 = new QHBoxLayout(this);

	QLabel* lblQuantite = new QLabel("Quantite");

	lblLigne2->addWidget(lblQuantite);
	colonne1->addLayout(lblLigne2);

	//deuxieme rang�
	QHBoxLayout* ligne2 = new QHBoxLayout(this);
	quantite = new QComboBox(this);
	for (int i = 1; i < 100; i++) {
		quantite->addItem(QString::number(i));
	}
	ligne2->addWidget(quantite);
	colonne1->addLayout(ligne2);


	//bouton de fin
	btnAjouter = new QPushButton(this);
	btnAjouter->setText("Ajouter quantite");
	colonne1->addWidget(btnAjouter);


	//bouton fini
	btnFini = new QPushButton(this);
	btnFini->setText("Fin des quantite");
	colonne1->addWidget(btnFini);

	connect(btnFini, SIGNAL(clicked()), parent, SLOT(allerIG()));
	connect(btnAjouter, SIGNAL(clicked()), this, SLOT(ajouterQuantite()));

}

void AjouterQuantite::ajouterQuantite() {
	//�cire le code pour modifier la quantite d'un produit
}