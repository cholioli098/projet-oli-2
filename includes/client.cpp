#include "client.h"

Client::Client()
{

}

QString Client::obtenirId() {
	return id;
}
QString Client::obtenirNom() {
	return nom;
}
QString Client::obtenirPrenom() {
	return prenom;
}

void Client::definirId(QString _id) {
	id = _id;
}
void Client::definirNom(QString _nom) {
	nom = _nom;
}
void Client::definirPrenom(QString _prenom) {
	prenom = _prenom;
}

Client::~Client()
{
}