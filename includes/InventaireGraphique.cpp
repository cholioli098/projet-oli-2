#include "InventaireGraphique.h"

InventaireGraphique::InventaireGraphique(QWidget* parent) : QWidget(parent)
{
	QVBoxLayout* colonne1 = new QVBoxLayout(this);

	//Le tableau a phil


	// les boutons
	QHBoxLayout* ligne1 = new QHBoxLayout(this);
	colonne1->addLayout(ligne1);

	btnAjouterProduit = new QPushButton(this);
	btnAjouterProduit->setText("Nouveau produit");
	ligne1->addWidget(btnAjouterProduit);

	btnAjouterQuantite = new QPushButton(this);
	btnAjouterQuantite->setText("Ajouter Quantite");
	ligne1->addWidget(btnAjouterQuantite);

	btnBack = new QPushButton(this);
	btnBack->setText("Back");
	colonne1->addWidget(btnBack);

	setLayout(colonne1);

	connect(btnBack, SIGNAL(clicked()), parent, SLOT(allerMP()));
	connect(btnAjouterProduit, SIGNAL(clicked()), parent, SLOT(allerAP()));
	connect(btnAjouterQuantite, SIGNAL(clicked()), parent, SLOT(allerAQ()));

}