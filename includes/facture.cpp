#include "facture.h"

Facture::Facture()
{

}

bool Facture::enregistrer()
{
    QString requeteString = "INSERT INTO facture(numero, commentaires, client_id, date, actif) VALUES (?, ?, ?, DATETIME('now', '-4 hours'), ?);";
    QSqlQuery requete;
    requete.prepare(requeteString);
    // TODO Valider avant
    requete.addBindValue(numero);
    requete.addBindValue(commentaires);
    requete.addBindValue(clientId);
    requete.addBindValue(actif);

    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
        return false;
    }

    id = requete.lastInsertId().toString();
    return true;
}

bool Facture::supprimer()
{
    if (id != 0) {
        QString requeteString = "DELETE FROM facture WHERE id = ?";
        QSqlQuery requete;
        requete.prepare(requeteString);
        // TODO Valider avant
        requete.addBindValue(id);

        if (!requete.exec()) {
            qWarning() << "ERROR: " << requete.lastError().text();
            return true;
        }
        return true;
    }
    return true;
}

#pragma region Getters et setters

QString Facture::obtenirId() {
    return id;
}
QString Facture::obtenirNumero() {
    return numero;
}
QString Facture::obtenirCommentaires() {
    return commentaires;
}
QString Facture::obtenirClientId() {
    return clientId;
}
QDateTime Facture::obtenirDate() {
    return date;
}
bool Facture::obtenirActif() {
    return actif;
}

Client Facture::obtenirClient() {
    return client;
}

void Facture::definirId(QString _id) {
    id = _id;
}
void Facture::definirNumero(QString _numero) {
    numero = _numero;
}
void Facture::definirCommentaires(QString _commentaires) {
    commentaires = _commentaires;
}
void Facture::definirClientId(QString _clientId) {
    clientId = _clientId;
}
void Facture::definirDate(QDateTime _date) {
    date = _date;
}
void Facture::definirActif(bool _actif) {
    actif = _actif;
}

void Facture::definirClient(Client _client) {
    client = _client;
}

#pragma endregion

// Obtenir une facture � partir de son identifiant.
Facture Facture::obtenirFactureParId(QString id) {

    Facture facture;
    QString requeteString = "SELECT * FROM facture WHERE id = ?;";
    QSqlQuery requete;
    requete.prepare(requeteString);
    requete.addBindValue(id);

    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
    }
    if (requete.first()) {
        facture.id = requete.value(0).toString();
        facture.numero = requete.value(1).toString();
        facture.commentaires = requete.value(2).toString();
        facture.clientId = requete.value(3).toString();
        facture.date = requete.value(4).toDateTime();
        facture.actif = requete.value(5).toBool();
    }
    return facture;
}

// Rechercher les factures dans un intervale de temps.
QVector<Facture> Facture::rechercher(QString dateDebut, QString dateFin)
{
    QVector<Facture> factures;
    QString requeteString = "SELECT facture.id, facture.numero, facture.commentaires, client.nom, client.prenom, facture.date, facture.actif FROM facture INNER JOIN client on client_id = client.id WHERE date >= ? AND date <= ?";
    QSqlQuery requete;
    requete.prepare(requeteString);
    requete.addBindValue(dateDebut + " 00:00:00");
    requete.addBindValue(dateFin + "11:59:59");


    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
    }
    while (requete.next())
    {
        Facture facture;
        Client client;
        facture.definirId(requete.value(0).toString());
        facture.definirNumero(requete.value(1).toString());
        facture.definirCommentaires(requete.value(2).toString());
        facture.definirDate(requete.value(5).toDateTime());
        facture.definirActif(requete.value(6).toBool());
        client.definirNom(requete.value(3).toString());
        client.definirPrenom(requete.value(4).toString());
        facture.definirClient(client);
        factures.push_back(facture);
    }
    return factures;
}

Facture::~Facture()
{
}