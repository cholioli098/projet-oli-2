﻿#include "FenetreFacture.h"

AjouterFacture::AjouterFacture(QWidget(*parent)): QWidget(parent) {
	
	QVBoxLayout* colonne1 = new QVBoxLayout(this);
	indexLigneTableau = 0;
	indexColonneTableau = 0;

	//Rangé de lbl
	QHBoxLayout* lblLigne1 = new QHBoxLayout(this);
	QLabel* lblNom = new QLabel("Nom");
	QLabel* lblCouleur = new QLabel("Couleur");
	lblLigne1->addWidget(lblNom);
	lblLigne1->addWidget(lblCouleur);
	colonne1->addLayout(lblLigne1);

	//premiere rangé
	QHBoxLayout* ligne1 = new QHBoxLayout(this);
	nom = new QComboBox(this);
	nom->addItem("Choisir...", 0);
	// faire une boucle et ajouter les nom de produit
	QVector<Produit> produits = Produit::obtenirTousLesProduits();
	QVectorIterator<Produit> produitsIterateur(produits);

	for (int i = 0; i < produits.length(); i++) {
		nom->addItem(produits[i].obtenirNom(), produits[i].obtenirId());
	}

	couleur = new QComboBox(this);
	couleur->addItem("Choisir...", 0);
	// faire une boucle et ajouter les couleur avec le meme nom du produit
	couleur->addItem("rouge");
	couleur->addItem("bleu");


	ligne1->addWidget(nom);
	ligne1->addWidget(couleur);
	colonne1->addLayout(ligne1);

	//Rangé de lbl
	QHBoxLayout* lblLigne2 = new QHBoxLayout(this);
	
	QLabel* lblQuantite = new QLabel("Quantite");
	
	lblLigne2->addWidget(lblQuantite);
	colonne1->addLayout(lblLigne2);

	//deuxieme rangé
	QHBoxLayout* ligne2 = new QHBoxLayout(this);
	quantite = new QComboBox(this);
	for(int i =1; i< 100;i++){
		quantite->addItem(QString::number(i));
	}
	ligne2->addWidget(quantite);
	colonne1->addLayout(ligne2);


	//bouton de fin
	btnAjouter = new QPushButton(this);
	btnAjouter->setText("Ajouter le produit");
	colonne1->addWidget(btnAjouter);

	//listes des produits 
	tableWidget = new QTableWidget(0, 4);
	QStringList enTeteHorizontalTableau = { "Nom", "Couleur", "Quantite", "Actions" };
	tableWidget->setHorizontalHeaderLabels(enTeteHorizontalTableau);
	tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	colonne1->addWidget(tableWidget);

	//bouton fini
	btnFini = new QPushButton(this);
	btnFini->setText("Fin de la facture");
	colonne1->addWidget(btnFini);

	//bouton annuler
	btnAnnulerFacture = new QPushButton(this);
	btnAnnulerFacture->setText("Annuler Facture");
	colonne1->addWidget(btnAnnulerFacture);

	setLayout(colonne1);
	

	//connection des fonctions
	connect(tableWidget, &QTableWidget::cellChanged, this, &AjouterFacture::celluleModifieeSlot);
	connect(tableWidget, &QTableWidget::cellClicked, this, &AjouterFacture::celluleCliqueeSlot);
	connect(btnAjouter, SIGNAL(clicked()), this, SLOT(ajouterProduitSlot()));
	connect(btnFini, SIGNAL(clicked()), this, SLOT(enregistrerFactureSlot()));
	connect(btnAnnulerFacture, SIGNAL(clicked()), parent, SLOT(allerFG()));
}

void AjouterFacture::enregistrerFactureSlot()
{
	Facture facture;

	// TODO Numéro ? Commentaires ? Client ?
	bool reussi = true;
	int tempRandomNumero = rand() % 100;
	facture.definirNumero(QString::number(tempRandomNumero));
	facture.definirCommentaires("");
	facture.definirClientId("1");
	facture.definirActif(true);

	if (facture.enregistrer())
	{
		for (int i = 0; i < factureProduits.length(); i++) {
			factureProduits[i].definirFactureId(facture.obtenirId());
			if(!factureProduits[i].enregistrer()){
				reussi = false;
			}
		}
	}
	else {
		MainWindow::afficherBoiteMessage("L'enregistrement a échoué.", QMessageBox::Critical);
		return;
	}
	if (reussi) {
		MainWindow::afficherBoiteMessage("Enregistrement réussi.", QMessageBox::Information);
		// Réinitialiser le formulaire.
		tableWidget->setRowCount(0);
		nom->setCurrentIndex(0);
		couleur->setCurrentIndex(0);
		quantite->setCurrentIndex(0);
		return;
	}
	else {
		for (int i = 0; i < factureProduits.length(); i++) {
			factureProduits[i].supprimer();
		}
		MainWindow::afficherBoiteMessage("L'enregistrement a échoué.", QMessageBox::Critical);
	}
}

void AjouterFacture::celluleModifieeSlot(int ligne, int colonne)
{
	// Si on modifie la quantité d'un article dans le tableau.
	int indexColonneQuantite = 2;
	if (colonne == indexColonneQuantite)
	{
		int nouvelleQuantite = tableWidget->item(ligne, colonne)->text().toInt();
		factureProduits[ligne].definirQuantite(nouvelleQuantite);
	}
}

void AjouterFacture::celluleCliqueeSlot(int ligne, int colonne)
{
	// Si on clique sur "Retirer".
	int indexColonneRetirer = 3;
	if (colonne == indexColonneRetirer) {
		factureProduits.removeAt(ligne);
		indexLigneTableau--;
		tableWidget->removeRow(ligne);
	}
}

void AjouterFacture::ajouterProduitSlot() {
	QString produitId = nom->currentData(Qt::UserRole).toString();
	QString couleurIndex = couleur->currentData(Qt::UserRole).toString();

	// Valider les listes déroulantes (pas à l'option "Choisir...")
	if (produitId == "0" || couleurIndex == "0")
	{
		MainWindow::afficherBoiteMessage("Les champs ne sont pas remplis correctement.", QMessageBox::Warning);
		return;
	}

	QString nomProduitString = nom->currentText();
	QString couleurProduiString = couleur->currentText();
	QString quantiteProduitString = quantite->currentText();

	// Insérer l'ajout dans un vecteur qui permettra d'insérer dans la base de données.
	FactureProduit factureProduit;
	factureProduit.definirProduitId(produitId);
	factureProduit.definirQuantite(quantiteProduitString.toInt());
	factureProduit.definirCouleur(couleurProduiString);
	factureProduit.definirActif(true);
	factureProduits.push_back(factureProduit);

	tableWidget->insertRow(indexLigneTableau);
	QTableWidgetItem* nomProduitItem = new QTableWidgetItem(nomProduitString);
	nomProduitItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	QTableWidgetItem* couleurtItem = new QTableWidgetItem(couleurProduiString);
	couleurtItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
	QTableWidgetItem* retirerItem = new QTableWidgetItem("Retirer");
	retirerItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);

	// Insérer dans le tableau.
	tableWidget->setItem(indexLigneTableau, indexColonneTableau++, nomProduitItem);
	tableWidget->setItem(indexLigneTableau, indexColonneTableau++, couleurtItem);
	tableWidget->setItem(indexLigneTableau, indexColonneTableau++, new QTableWidgetItem(quantiteProduitString));
	tableWidget->setItem(indexLigneTableau, indexColonneTableau++, retirerItem);
	// Incrémenter l'index de la ligne actuelle du tableau.
	indexLigneTableau++;
	indexColonneTableau = 0;
}

//********************************************************************

//********************************************************************
ChargerFacture::ChargerFacture(QWidget(*parent)):QWidget(parent) {
	
	QVBoxLayout* colonne1 = new QVBoxLayout(this);
	
	//Label du début
	QLabel* lblDebut = new QLabel("Debut");
	//Label de la fin
	QLabel* lblFin = new QLabel("Fin");

	//date début
	dDate = new QDateEdit(this);

	//date fin
	fDate = new QDateEdit(this);

	//le bouton
	btnFini = new QPushButton(this);
	btnFini->setText("rechercher");

	//bouton back
	btnBack = new QPushButton(this);
	btnBack->setText("Back");

	colonne1->addWidget(lblDebut);
	colonne1->addWidget(dDate);
	colonne1->addWidget(lblFin);
	colonne1->addWidget(fDate);
	colonne1->addWidget(btnFini);
	colonne1->addWidget(btnBack);

	setLayout(colonne1);

	//connection bouton
	connect(btnFini, SIGNAL(clicked()), this, SLOT(chercherFacture()));
	connect(btnBack, SIGNAL(clicked()), parent, SLOT(allerFG()));
}

void ChargerFacture::chercherFacture() {

	// TODO peut-être utiliser des QDateTime au lieu de QString comme paramètres à la méthode rechercher.
	QVector<Facture> factures = Facture::rechercher(dDate->text(), fDate->text());
	QString test;
	// Looper sur les factures.
}

