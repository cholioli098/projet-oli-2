
#include "factureGraphique.h"

FactureGraphique::FactureGraphique(QWidget* parent) : QWidget(parent) 
{

	
	QVBoxLayout* colonne1 = new QVBoxLayout();

	//Cr�ation de bouton
	 QSize btnSize = QSize(200, 300);

	btnAjouterFacture = new QPushButton();
	btnAjouterFacture->setText("Ajouter une facture");
	btnAjouterFacture->resize(btnSize);

	btnChargerFacture = new QPushButton();
	btnChargerFacture->setText("Charger des factures");
	btnChargerFacture->resize(btnSize);


	btnModifierFacture = new QPushButton();
	btnModifierFacture->setText("Modifier une facture");
	btnModifierFacture->resize(btnSize);

	btnBack = new QPushButton();
	btnBack->setText("Back");

	// ajouter les bouton au Vbox
	colonne1->addWidget(btnAjouterFacture);
	colonne1->addWidget(btnChargerFacture);
	colonne1->addWidget(btnModifierFacture);
	colonne1->addWidget(btnBack);

	
	setLayout(colonne1);
	//les connects
	connect(btnAjouterFacture, SIGNAL(clicked()), parent, SLOT(allerAF()));
	connect(btnChargerFacture, SIGNAL(clicked()), parent, SLOT(allerCF()));
	connect(btnBack, SIGNAL(clicked()), parent, SLOT(allerMP()));

}

