#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "qvariant.h"
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	MainWindow();
	QStackedWidget* layout;
	static void afficherBoiteMessage(QString message, QMessageBox::Icon);
public slots:
	void changerLayout(int index);
	void allerFG();
	void allerAF();
	void allerCF();
	void allerMP();
	void allerIG();
	void allerAP();
	void allerAQ();
	bool connecterBaseDeDonnees();
private:
	QWidget* widget;
	QSqlDatabase bd;
};
#endif // MAINWINDOW_H
