#include "factureProduit.h"

FactureProduit::FactureProduit()
{
}

bool FactureProduit::enregistrer()
{
    QString requeteString = "INSERT INTO facture_produit(facture_id, produit_id, quantite, actif, couleur) VALUES (?, ?, ?, ?, ?);";
    QSqlQuery requete;
    requete.prepare(requeteString);
    // TODO Valider avant
    requete.addBindValue(factureId);
    requete.addBindValue(produitId);
    requete.addBindValue(quantite);
    requete.addBindValue(actif);
    requete.addBindValue(couleur);

    if (!requete.exec()) {
        qWarning() << "ERROR: " << requete.lastError().text();
        return false;
    }

    id = requete.lastInsertId().toString();
    return true;
}

bool FactureProduit::supprimer()
{
    if (id != 0) {
        QString requeteString = "DELETE FROM facture_produit WHERE id = ?;";
        QSqlQuery requete;
        requete.prepare(requeteString);
        // TODO Valider avant
        requete.addBindValue(id);

        if (!requete.exec()) {
            qWarning() << "ERROR: " << requete.lastError().text();
            return true;
        }

        id = requete.lastInsertId().toString();
        return true;
    }
    return true;
}

#pragma region Getters et setters

QString FactureProduit::obtenirId() {
	return id;
}
QString FactureProduit::obtenirFactureId() {
	return factureId;
}
QString FactureProduit::obtenirProduitId() {
	return produitId;
}
QString FactureProduit::obtenirCouleur() {
	return couleur;
}
int FactureProduit::obtenirQuantite() {
	return quantite;
}
bool FactureProduit::obtenirActif() {
	return actif;
}

void FactureProduit::definirId(QString _id) {
	id = _id;
}
void FactureProduit::definirFactureId(QString _factureId) {
	factureId = _factureId;
}
void FactureProduit::definirProduitId(QString _produitId) {
	produitId = _produitId;
}
void FactureProduit::definirCouleur(QString _couleur) {
	couleur = _couleur;
}
void FactureProduit::definirQuantite(int _quantite) {
	quantite = _quantite;
}
void FactureProduit::definirActif(bool _actif) {
	actif = _actif;
}

#pragma endregion

FactureProduit::~FactureProduit()
{

}