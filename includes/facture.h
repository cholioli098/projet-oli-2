#include <qstring.h>
#include <qdatetime.h>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>
#include "client.h"

class Facture
{
public:
	Facture();
	~Facture();
	static Facture obtenirFactureParId(QString id);
	static QVector<Facture> rechercher(QString dateDebut, QString dateFin);
	bool enregistrer();
	bool supprimer();

	QString obtenirId();
	QString obtenirNumero();
	QString obtenirCommentaires();
	QString obtenirClientId();
	QDateTime obtenirDate();
	bool obtenirActif();
	Client obtenirClient();

	void definirId(QString _id);
	void definirNumero(QString _numero);
	void definirCommentaires(QString _commentaires);
	void definirClientId(QString _clientId);
	void definirDate(QDateTime _date);
	void definirActif(bool _actif);
	void definirClient(Client _client);


private:		
	QString id;
	QString numero;
	QString commentaires;
	QString clientId;
	QDateTime date;
	bool actif;
	Client client;
};