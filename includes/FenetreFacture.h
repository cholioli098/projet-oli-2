#ifndef AJOUTERFACTURE_H
#define AJOUTERFACTURE_H

#include <QMainWindow>
#include <QtWidgets>
#include <qvector.h>
#include <qvector.h>
#include <qtablewidget.h>
#include "produit.h"
#include "factureProduit.h"
#include "facture.h"
#include "mainwindow.h"


class AjouterFacture : public QWidget
{
	Q_OBJECT
public:
	
	AjouterFacture(QWidget(*parent));

private:
	QPushButton* btnAjouter;
	QPushButton* btnFini;
	QComboBox* nom;
	QComboBox* quantite;
	QComboBox* couleur;
	QPushButton* btnAnnulerFacture;
	QTableWidget* tableWidget;
	QVector<FactureProduit> factureProduits;
	int indexLigneTableau;
	int indexColonneTableau;

private slots:
	void celluleModifieeSlot(int ligne, int colonne);
	void celluleCliqueeSlot(int ligne, int colonne);
	void enregistrerFactureSlot();
	void ajouterProduitSlot();
};
#endif
//********************************************************************




//********************************************************************
#ifndef CHARGERFACTURE_H
#define CHARGERFACTURE_H

#include <QMainWindow>
#include <QtWidgets>

class ChargerFacture : public QWidget
{
	Q_OBJECT
public:
	ChargerFacture(QWidget(*parent));
	
private:    
	QLineEdit* dJour;
	QLineEdit* dMois;
	QLineEdit* dAnnee;
	QLineEdit* fJour;
	QLineEdit* fMois;
	QLineEdit* fAnnee;
	QDateEdit* dDate;
	QDateEdit* fDate;

	QPushButton* btnFini;
	QPushButton* btnBack;
private slots:
	void chercherFacture();
};
#endif
