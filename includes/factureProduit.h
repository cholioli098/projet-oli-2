#include <qstring.h>
#include <qvector.h>
#include <qsqlquery.h>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

class FactureProduit
{
public:
	FactureProduit();
	~FactureProduit();

	QString obtenirId();
	QString obtenirFactureId();
	QString obtenirProduitId();
	QString obtenirCouleur();
	int obtenirQuantite();
	bool obtenirActif();

	void definirId(QString _id);
	void definirFactureId(QString _factureId);
	void definirProduitId(QString _produitId);
	void definirCouleur(QString _couleur);
	void definirQuantite(int _quantite);
	void definirActif(bool _actif);

	bool enregistrer();
	bool supprimer();

private:
	QString id;
	QString factureId;
	QString produitId;
	QString couleur;
	int quantite;
	bool actif;

};