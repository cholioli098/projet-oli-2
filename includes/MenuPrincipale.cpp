#include "MenuPrincipale.h"



MenuPrincipale::MenuPrincipale(QMainWindow * parent) : QWidget(parent)
{
	//this->setWindowTitle(titre);
	QPixmap bkgnd(":/images/Fond.png");  //Load pic
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio); //set scale of pic to match the window
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);//set the pic to the background
	this->setPalette(palette); //show the background pic

	QFont font;
	font.setPointSize(20);
	
	QVBoxLayout* layout = new QVBoxLayout;


	QPixmap pix(":/images/ICON.png");
	QLabel* Icon = new QLabel;
	Icon->setPixmap(pix);
	Icon->setAlignment(Qt::AlignCenter);
	QHBoxLayout* layoutBouton = new QHBoxLayout;

	QPushButton* boutonInventaire = new QPushButton;
	boutonInventaire->setText(tr("Inventaire"));
	boutonInventaire->setFont(font);
	boutonInventaire->setGeometry(20, 20, 1, 1);
	layoutBouton->addWidget(boutonInventaire);

	QPushButton* boutonFacture = new QPushButton;
	boutonFacture->setText(tr("Facture"));
	boutonFacture->setFont(font);
	boutonFacture->setGeometry(20, 20, 1, 1);
	layoutBouton->addWidget(boutonFacture);

	QWidget* widgBouton = new QWidget;
	widgBouton->setLayout(layoutBouton);

	layout->addWidget(Icon, Qt::IgnoreAspectRatio);
	layout->addWidget(widgBouton);
	this->setLayout(layout);
	//mettre le bon layout pour l'inventaire
	//connect(boutonInventaire, SIGNAL(clicked()), parent, SLOT(changerLayout(2)));
	connect(boutonFacture, SIGNAL(clicked()), parent, SLOT(allerFG()));
	connect(boutonInventaire, SIGNAL(clicked()), parent, SLOT(allerIG()));
}

void MenuPrincipale::resizeEvent(QResizeEvent* evt)
{
	QPixmap bkgnd(":/images/Fond.png");//Load pic
	bkgnd = bkgnd.scaled(size(), Qt::IgnoreAspectRatio);//set scale of pic to match the window
	QPalette p = palette(); //copy current, not create new
	p.setBrush(QPalette::Background, bkgnd);//set the pic to the background
	setPalette(p);//show the background pic

	QWidget::resizeEvent(evt); //call base implementation
}
