#include <qstring.h>
class Client
{
public:
	Client();
	~Client();

	QString obtenirId();
	QString obtenirNom();
	QString obtenirPrenom();

	void definirId(QString _id);
	void definirNom(QString _id);
	void definirPrenom(QString _id);

private:
	QString id;
	QString nom;
	QString prenom;
};