#ifndef AJOUTERPRODUIT_H
#define AJOUTERPRODUIT_H

#include <QtWidgets>
#include <QMainWindow>

class AjouterProduit : public QWidget
{
	Q_OBJECT

public:
	AjouterProduit(QWidget(*parent));


private:
	QPushButton* btnAjouter;
	QLineEdit* nom;
	QComboBox* quantite;
	QLineEdit* couleur;
	QLineEdit* prix;
	QPushButton* btnBack;

private slots:
	void ajouterProduit();

};
#endif // !AJOUTERPRODUIT_H

#ifndef AJOUTERQUANTITE_H
#define AJOUTERQUANTITE_H

class AjouterQuantite : public QWidget
{
	Q_OBJECT
public:
	AjouterQuantite(QWidget(*parent));

private: 
	QComboBox* nom;
	QComboBox* quantite;
	QComboBox* couleur;
	QPushButton* btnAjouter;
	QPushButton* btnFini;

private slots:
	void ajouterQuantite();


};
#endif // !AJOUTERQUANTITE_H

