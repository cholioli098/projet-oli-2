#ifndef MENUPRINCIPALE_H
#define MENUPRINCIPALE_H

#include <QMainWindow>
#include <QtWidgets>

class MenuPrincipale : public QWidget
{
	Q_OBJECT
	
	
public:
	MenuPrincipale(QMainWindow* parent);
	void resizeEvent(QResizeEvent* evt);
	

private slots:
	

private:
	QWidget* widget;
};
#endif // MAINWINDOW_H

