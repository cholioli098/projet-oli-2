#include <qstring.h>
#include <qvector.h>
#include <qdatetime.h>
#include <qsqlquery.h>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

class Produit
{
public:
	Produit();
	~Produit();
	QString obtenirId();
	QString obtenirNom();
	QString obtenirDescription();
	QString obtenirCouleur();
	double obtenirPrixUnitaire();
	int obtenirQuantiteInventaire();
	bool obtenirActif();

	void definirId(QString _id);
	void definirNom(QString _nom);
	void definirDescription(QString _description);
	void definirCouleur(QString _couleur);
	void definirPrixUnitaire(double _prixUnitaire);
	void definirQuantiteInventaire(int _quantiteInventaire);
	void definirActif(bool _actif);

	static QVector<Produit> obtenirTousLesProduits();
	bool enregistrer();
	bool retirerQuantiteInventaire(int quantiteARetirer);

private:
	QString id;
	QString nom;
	QString description;
	QString couleur;
	double prixUnitaire;
	int quantiteInventaire;
	bool actif;


};