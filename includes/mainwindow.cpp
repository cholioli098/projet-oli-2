#include "mainwindow.h"
#include <QMainWindow>
#include <includes\factureGraphique.h>
#include <includes\FenetreFacture.h>
#include <includes\MenuPrincipale.h>
#include <includes\InventaireGraphique.h>
#include <includes\FenetreInventaire.h>

MainWindow::MainWindow()
{
	layout = new QStackedWidget();
	connecterBaseDeDonnees();

	MenuPrincipale* MP = new MenuPrincipale(this);
	FactureGraphique* FG = new FactureGraphique(this);
	ChargerFacture* CF = new ChargerFacture(this);
	AjouterFacture* AF = new AjouterFacture(this);
	InventaireGraphique* IG = new InventaireGraphique(this);
	AjouterProduit* AP = new AjouterProduit(this);
	AjouterQuantite* AQ = new AjouterQuantite(this);


	//ajouter au layout
	layout->addWidget(MP);
	layout->addWidget(FG);
	layout->addWidget(CF);
	layout->addWidget(AF);
	layout->addWidget(IG);
	layout->addWidget(AP);
	layout->addWidget(AQ);
	layout->setCurrentIndex(0);
	//this->setLayout(layout);
	setCentralWidget(layout);
}

void MainWindow::afficherBoiteMessage(QString message, QMessageBox::Icon icon)
{
	QMessageBox msgBox;
	msgBox.setText(message);
	msgBox.setIcon(icon);
	msgBox.exec();
}

void MainWindow::changerLayout(int index) {
	layout->setCurrentIndex(index);
}

void MainWindow::allerFG() {
	layout->setCurrentIndex(1);
}

void MainWindow::allerCF() {
	layout->setCurrentIndex(2);
}

void MainWindow::allerAF() {
	layout->setCurrentIndex(3);
}

void MainWindow::allerMP() {
	layout->setCurrentIndex(0);
}

void MainWindow::allerIG() {
	layout->setCurrentIndex(4);
}

void MainWindow::allerAP() {
	layout->setCurrentIndex(5);
}

void MainWindow::allerAQ() {
	layout->setCurrentIndex(6);
}

bool MainWindow::connecterBaseDeDonnees() {
	bd = QSqlDatabase::addDatabase("QSQLITE");
	bd.setDatabaseName("./data/projetMax.db");

	if (!bd.open())
	{
		qWarning() << "ERROR: " << bd.lastError();
		return false;
	}
	return true;
}
