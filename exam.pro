TEMPLATE = vcapp
TARGET = projetOliCONFIG
INCLUDEPATH += ./includes
CONFIG += warn_on qt debug console debug_and_release window
HEADERS += $$files(includes/*.h)
SOURCES += $$files(includes/*.cpp)
QT += widgets sql core
RESOURCES += \
    ressources.qrc
